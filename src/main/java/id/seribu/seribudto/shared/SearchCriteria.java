package id.seribu.seribudto.shared;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class SearchCriteria {
    @NonNull
    private String key;
    @NonNull
    private String operation;
    @NonNull
    private Object value;
    @NonNull
    private boolean orPredicate;
}