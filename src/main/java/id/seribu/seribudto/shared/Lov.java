package id.seribu.seribudto.shared;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Lov implements Serializable{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @NonNull
    private String value;
    @NonNull
    private String text;
}