package id.seribu.seribudto.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class AuthToken{
    
    @NonNull
    private String token;
    @NonNull
    private String userId;
}