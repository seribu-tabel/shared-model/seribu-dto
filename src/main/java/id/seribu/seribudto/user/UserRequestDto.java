package id.seribu.seribudto.user;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import lombok.Data;

/**
 * @author arieki
 */
@Data
public class UserRequestDto implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private UUID id;
    private String username;
    private String name;
    private String nip;
    private String password;
    private String role;
    private boolean enabled;
    private Date createdDate;
}