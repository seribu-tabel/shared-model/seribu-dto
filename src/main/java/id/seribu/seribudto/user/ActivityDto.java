package id.seribu.seribudto.user;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author arieki
 */
@Data
@NoArgsConstructor
public class ActivityDto implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String username;
    private String nip;
    private String authority;
    private String ipAddress;
    private String activity;
    private String dataset;
    private String subyekData;
    private String sourceData;
    private String firstCharacter;
    private String secondCharacter;
    private Date accessDate;
}