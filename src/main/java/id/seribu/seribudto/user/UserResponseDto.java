package id.seribu.seribudto.user;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * @author arieki
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class UserResponseDto implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @NonNull
    private UUID id;
    @NonNull
    private String nip;
    @NonNull
    private String name;
    @NonNull
    private String username;
    @NonNull
    private String role;
    @NonNull
    private String roleText;
    @NonNull
    private String password;
    @NonNull
    private Boolean enabled;
    @NonNull
    private Date createdDate;
}