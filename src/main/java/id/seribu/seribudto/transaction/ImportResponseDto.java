package id.seribu.seribudto.transaction;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class ImportResponseDto {

    @NonNull
    private String dataset;
    @NonNull
    private String owner;    
    @NonNull
    private String fileName;
    @NonNull
    private String fileUri;
    @NonNull
    private String fileType;
    @NonNull
    private long size;
}