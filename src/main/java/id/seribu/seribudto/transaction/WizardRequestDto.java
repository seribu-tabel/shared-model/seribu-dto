package id.seribu.seribudto.transaction;

import lombok.Data;

@Data
public class WizardRequestDto {

    private String dataset;
    private String subyekData;
    private String sourceData;
    private String[] rowPivot;
    private String columnPivot;
    private String[] filterByYear;
}