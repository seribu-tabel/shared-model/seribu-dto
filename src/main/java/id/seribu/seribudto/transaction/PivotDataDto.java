package id.seribu.seribudto.transaction;

import java.util.Date;
import java.util.UUID;

import lombok.Data;

@Data
public class PivotDataDto {
    public UUID id;
	public String tahun;
	public String kodeKpp;
	public String namaKpp;
	public String kanwil;
	public String provinsi;
	public String pulau;
	public String kodeKategori;
	public String namaKategori;
	public String statusPerkawinan;
	public String jumlahTanggungan;
	public String jenisOp;
	public String rangeUsia;
	public String jenisKelamin;
	public String wargaNegara;
	public String kategoriWp;
	public String jenisWp;
	public String modal;
	public String statusPusat;
	public String periodePembukuan;
	public String tahunPendirian;
	public String badanHukum;
	public String jenis;
	public String statusNpwp;
	public String statusPkp;
	public String metodePembukuan;
	public int jumlah;
	private String createdBy;
	private Date createdDate;
}