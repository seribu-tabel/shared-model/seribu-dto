package id.seribu.seribudto.transaction;

import org.springframework.core.io.Resource;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class DownloadResponseDto {
    @NonNull
    private String fileType;
    @NonNull
    private Resource resource;
}