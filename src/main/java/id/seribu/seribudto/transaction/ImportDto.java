package id.seribu.seribudto.transaction;

import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class ImportDto {
    @NonNull
    private String dataset;
    @NonNull
    private String subyekData;
    @NonNull
    private String sourceData;
    @NonNull
    private String directory;
    @NonNull
    private String filename;
    @NonNull
    private String uploadedBy;
    @NonNull
    private Date uploadedDate;
}