package id.seribu.seribudto.transaction;

import java.util.UUID;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class UploadRequestDto {

    @NonNull
    private UUID id;
    @NonNull
    private String dataset;
    @NonNull
    private String owner;
    @NonNull
    private String directory;
    @NonNull
    private String uploadedBy;
    @NonNull
    private Integer releaseYear;
    @NonNull
    private String description;

}