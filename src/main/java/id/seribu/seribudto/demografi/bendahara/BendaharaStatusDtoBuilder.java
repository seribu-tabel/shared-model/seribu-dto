package id.seribu.seribudto.demografi.bendahara;

import java.util.Date;
import java.util.UUID;

import id.seribu.seribudto.demografi.DemografiDto;
import lombok.Getter;

@Getter
public class BendaharaStatusDtoBuilder {

    private UUID id;
	private String tahun;
	private String kodeKpp;
	private String namaKpp;
	private String kanwil;
	private String provinsi;
    private String pulau;
    private String jenis;
	private String statusNpwp;
	private String statusPkp;
	private int jumlah;
	private String createdBy;
	private Date createdDate;

    public BendaharaStatusDtoBuilder id() {
        this.id = UUID.randomUUID();
        return this;
    }
    public BendaharaStatusDtoBuilder tahun (String tahun) {
        this.tahun = tahun;
        return this;
    }
    public BendaharaStatusDtoBuilder kodeKpp (String kodeKpp) {
        this.kodeKpp = kodeKpp;
        return this;
    }
    public BendaharaStatusDtoBuilder namaKpp (String namaKpp) {
        this.namaKpp = namaKpp;
        return this;
    }
    public BendaharaStatusDtoBuilder kanwil (String kanwil) {
        this.kanwil = kanwil;
        return this;
    }
    public BendaharaStatusDtoBuilder provinsi (String provinsi) {
        this.provinsi = provinsi;
        return this;
    }
    public BendaharaStatusDtoBuilder pulau (String pulau) {
        this.pulau = pulau;
        return this;
    }
    public BendaharaStatusDtoBuilder jenis (String jenis) {
        this.jenis = jenis;
        return this;
    }
    public BendaharaStatusDtoBuilder statusNpwp (String statusNpwp) {
        this.statusNpwp = statusNpwp;
        return this;
    }
    public BendaharaStatusDtoBuilder statusPkp (String statusPkp) {
        this.statusPkp = statusPkp;
        return this;
    }
    public BendaharaStatusDtoBuilder jumlah (int jumlah) {
        this.jumlah = jumlah;
        return this;
    }
    public BendaharaStatusDtoBuilder createdBy (String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    public BendaharaStatusDtoBuilder createdDate (Date createdDate) {
        this.createdDate = new Date();
        return this;
    }

    public DemografiDto build() {
        DemografiDto demografiDto = new DemografiDto(this);
        return demografiDto;
    }
}