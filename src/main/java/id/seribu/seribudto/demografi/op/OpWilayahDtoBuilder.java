package id.seribu.seribudto.demografi.op;

import java.util.Date;
import java.util.UUID;

import id.seribu.seribudto.demografi.DemografiDto;
import lombok.Getter;

@Getter
public class OpWilayahDtoBuilder {
    private UUID id;
	private String tahun;
	private String kodeKpp;
	private String namaKpp;
	private String kanwil;
	private String provinsi;
	private String pulau;
	private String statusPerkawinan;
	private String jumlahTanggungan;
	private String jenisOp;
	private String rangeUsia;
	private String jenisKelamin;
	private String wargaNegara;
	private int jumlah;
	private String createdBy;
    private Date createdDate;
    
    public OpWilayahDtoBuilder id() {
        this.id = UUID.randomUUID();
        return this;
    }

    public OpWilayahDtoBuilder tahun(String tahun) {
        this.tahun = tahun;
        return this;
    }

    public OpWilayahDtoBuilder kodeKpp(String kodeKpp) {
        this.kodeKpp = kodeKpp;
        return this;
    }

    public OpWilayahDtoBuilder namaKpp (String namaKpp) {
        this.namaKpp = namaKpp;
        return this;        
    }
	public OpWilayahDtoBuilder kanwil (String kanwil) {
        this.kanwil = kanwil;
        return this;
    }
	public OpWilayahDtoBuilder provinsi (String provinsi) {
        this.provinsi = provinsi;
        return this;
    }
	public OpWilayahDtoBuilder pulau (String pulau) {
        this.pulau = pulau;
        return this;
    }
	public OpWilayahDtoBuilder statusPerkawinan (String statusPerkawinan) {
        this.statusPerkawinan = statusPerkawinan;
        return this;
    }
	public OpWilayahDtoBuilder jumlahTanggungan (String jumlahTanggungan) {
        this.jumlahTanggungan = jumlahTanggungan;
        return this;
    }
	public OpWilayahDtoBuilder jenisOp (String jenisOp) {
        this.jenisOp = jenisOp;
        return this;
    }
	public OpWilayahDtoBuilder rangeUsia (String rangeUsia) {
        this.rangeUsia = rangeUsia;
        return this;
    }
	public OpWilayahDtoBuilder jenisKelamin (String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
        return this;
    }
	public OpWilayahDtoBuilder wargaNegara (String wargaNegara) {
        this.wargaNegara = wargaNegara;
        return this;
    }
	public OpWilayahDtoBuilder jumlah (int jumlah) {
        this.jumlah = jumlah;
        return this;
    }
	public OpWilayahDtoBuilder createdBy (String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    public OpWilayahDtoBuilder createdDate (Date createdDate) {
        this.createdDate = new Date();
        return this;
    } 

    public DemografiDto build() {
        DemografiDto demografiDto = new DemografiDto(this);
        return demografiDto;
    }
}