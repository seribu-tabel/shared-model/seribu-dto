package id.seribu.seribudto.template;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class ApiResponse<T> implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = -8735871914074774098L;
    @NonNull
    private Integer status;
    @NonNull
    private String message;
    @NonNull
    private T result;
}