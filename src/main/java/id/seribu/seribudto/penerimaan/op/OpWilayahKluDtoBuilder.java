package id.seribu.seribudto.penerimaan.op;

import java.util.Date;
import java.util.UUID;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import lombok.Getter;

@Getter
public class OpWilayahKluDtoBuilder {

    private UUID id;
    private String tahun;
	private String kodeKpp;
	private String namaKpp;
	private String kanwil;
	private String provinsi;
	private String pulau;
	private String kodeKategori;
	private String namaKategori;
	private int jumlah;
	private String createdBy;
    private Date createdDate;

    public OpWilayahKluDtoBuilder id() {
        this.id = UUID.randomUUID();
        return this;
    }
    
    public OpWilayahKluDtoBuilder tahun(String tahun) {
        this.tahun = tahun;
        return this;
    }

    public OpWilayahKluDtoBuilder kodeKpp(String kodeKpp) {
        this.kodeKpp = kodeKpp;
        return this;
    }

    public OpWilayahKluDtoBuilder namaKpp(String namaKpp) {
        this.namaKpp = namaKpp;
        return this;
    }

    public OpWilayahKluDtoBuilder kanwil(String kanwil) {
        this.kanwil = kanwil;
        return this;
    }

    public OpWilayahKluDtoBuilder provinsi(String provinsi) {
        this.provinsi = provinsi;
        return this;
    }

    public OpWilayahKluDtoBuilder pulau(String pulau) {
        this.pulau = pulau;
        return this;
    }

    public OpWilayahKluDtoBuilder kodeKategori(String kodeKategori) {
        this.kodeKategori = kodeKategori;
        return this;
    }

    public OpWilayahKluDtoBuilder namaKategori(String namaKategori) {
        this.namaKategori = namaKategori;
        return this;
    }

    public OpWilayahKluDtoBuilder jumlah(int jumlah) {
        this.jumlah = jumlah;
        return this;
    }

    public OpWilayahKluDtoBuilder createdBy (String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    public OpWilayahKluDtoBuilder createdDate (Date createdDate) {
        this.createdDate = new Date();
        return this;
    }

    public PenerimaanDto build() {
        PenerimaanDto penerimaanDto = new PenerimaanDto(this);
        return penerimaanDto;
    }
}