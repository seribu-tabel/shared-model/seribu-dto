package id.seribu.seribudto.penerimaan.op;

import java.util.Date;
import java.util.UUID;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import lombok.Getter;

@Getter
public class OpStatusDtoBuilder {

    private UUID id;
    private String tahun;
	private String kodeKpp;
	private String namaKpp;
	private String kanwil;
	private String provinsi;
	private String pulau;
	private String kodeKategori;
    private String namaKategori;
    private String statusNpwp;
    private String statusPkp;
    private String metodePembukuan;
	private int jumlah;
	private String createdBy;
    private Date createdDate;

    public OpStatusDtoBuilder id() {
        this.id = UUID.randomUUID();
        return this;
    }
    
    public OpStatusDtoBuilder tahun(String tahun) {
        this.tahun = tahun;
        return this;
    }

    public OpStatusDtoBuilder kodeKpp(String kodeKpp) {
        this.kodeKpp = kodeKpp;
        return this;
    }

    public OpStatusDtoBuilder namaKpp(String namaKpp) {
        this.namaKpp = namaKpp;
        return this;
    }

    public OpStatusDtoBuilder kanwil(String kanwil) {
        this.kanwil = kanwil;
        return this;
    }

    public OpStatusDtoBuilder provinsi(String provinsi) {
        this.provinsi = provinsi;
        return this;
    }

    public OpStatusDtoBuilder pulau(String pulau) {
        this.pulau = pulau;
        return this;
    }

    public OpStatusDtoBuilder kodeKategori(String kodeKategori) {
        this.kodeKategori = kodeKategori;
        return this;
    }

    public OpStatusDtoBuilder namaKategori(String namaKategori) {
        this.namaKategori = namaKategori;
        return this;
    }

    public OpStatusDtoBuilder statusNpwp(String statusNpwp) {
        this.statusNpwp = statusNpwp;
        return this;
    }

    public OpStatusDtoBuilder statusPkp(String statusPkp) {
        this.statusPkp = statusPkp;
        return this;
    }

    public OpStatusDtoBuilder metodePembukuan(String metodePembukuan) {
        this.metodePembukuan = metodePembukuan;
        return this;
    }

    public OpStatusDtoBuilder jumlah(int jumlah) {
        this.jumlah = jumlah;
        return this;
    }

    public OpStatusDtoBuilder createdBy (String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    public OpStatusDtoBuilder createdDate (Date createdDate) {
        this.createdDate = new Date();
        return this;
    }

    public PenerimaanDto build() {
        PenerimaanDto penerimaanDto = new PenerimaanDto(this);
        return penerimaanDto;
    }
}