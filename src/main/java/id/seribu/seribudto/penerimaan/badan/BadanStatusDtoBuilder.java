package id.seribu.seribudto.penerimaan.badan;

import java.util.Date;
import java.util.UUID;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import lombok.Getter;

@Getter
public class BadanStatusDtoBuilder {

    private UUID id;
	private String tahun;
	private String kodeKpp;
	private String namaKpp;
	private String kanwil;
	private String provinsi;
	private String pulau;
	private String kodeKategori;
    private String namaKategori;
    private String kategoriWp;
	private String modal;
	private String statusPusat;
	private String periodePembukuan;
	private String tahunPendirian;
	private String badanHukum;
	private String statusNpwp;
	private String statusPkp;
	private int jumlah;
	private String createdBy;
	private Date createdDate;

    public BadanStatusDtoBuilder id() {
        this.id = UUID.randomUUID();
        return this;
    }
    public BadanStatusDtoBuilder tahun (String tahun) {
        this.tahun = tahun;
        return this;
    }
    public BadanStatusDtoBuilder kodeKpp (String kodeKpp) {
        this.kodeKpp = kodeKpp;
        return this;
    }
    public BadanStatusDtoBuilder namaKpp (String namaKpp) {
        this.namaKpp = namaKpp;
        return this;
    }
    public BadanStatusDtoBuilder kanwil (String kanwil) {
        this.kanwil = kanwil;
        return this;
    }
    public BadanStatusDtoBuilder provinsi (String provinsi) {
        this.provinsi = provinsi;
        return this;
    }
    public BadanStatusDtoBuilder pulau (String pulau) {
        this.pulau = pulau;
        return this;
    }
    public BadanStatusDtoBuilder kodeKategori (String kodeKategori) {
        this.kodeKategori = kodeKategori;
        return this;
    }
    public BadanStatusDtoBuilder namaKategori (String namaKategori) {
        this.namaKategori = namaKategori;
        return this;
    }
    public BadanStatusDtoBuilder kategoriWp (String kategoriWp) {
        this.kategoriWp = kategoriWp;
        return this;
    }
    public BadanStatusDtoBuilder modal (String modal) {
        this.modal = modal;
        return this;
    }
    public BadanStatusDtoBuilder statusPusat (String statusPusat) {
        this.statusPusat = statusPusat;
        return this;
    }
    public BadanStatusDtoBuilder periodePembukuan (String periodePembukuan) {
        this.periodePembukuan = periodePembukuan;
        return this;
    }
    public BadanStatusDtoBuilder tahunPendirian (String tahunPendirian) {
        this.tahunPendirian = tahunPendirian;
        return this;
    }
    public BadanStatusDtoBuilder badanHukum (String badanHukum) {
        this.badanHukum = badanHukum;
        return this;
    }
    public BadanStatusDtoBuilder statusNpwp (String statusNpwp) {
        this.statusNpwp = statusNpwp;
        return this;
    }
    public BadanStatusDtoBuilder statusPkp (String statusPkp) {
        this.statusPkp = statusPkp;
        return this;
    }
    public BadanStatusDtoBuilder jumlah (int jumlah) {
        this.jumlah = jumlah;
        return this;
    }
    public BadanStatusDtoBuilder createdBy (String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    public BadanStatusDtoBuilder createdDate (Date createdDate) {
        this.createdDate = new Date();
        return this;
    }

    public PenerimaanDto build() {
        PenerimaanDto penerimaanDto = new PenerimaanDto(this);
        return penerimaanDto;
    }
}