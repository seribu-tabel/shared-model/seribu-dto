package id.seribu.seribudto.penerimaan.badan;

import java.util.Date;
import java.util.UUID;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import lombok.Getter;

@Getter
public class BadanAllDtoBuilder {

    private UUID id;
	private String tahun;
	private String kodeKpp;
	private String namaKpp;
	private String kanwil;
	private String provinsi;
	private String pulau;
	private String kodeKategori;
    private String namaKategori;
    private String kategoriWp;
	private String modal;   
	private String statusPusat;
	private String periodePembukuan;
	private String tahunPendirian;
	private String badanHukum;
	private int jumlah;
	private String createdBy;
	private Date createdDate;

    public BadanAllDtoBuilder id() {
        this.id = UUID.randomUUID();
        return this;
    }
    public BadanAllDtoBuilder tahun (String tahun) {
        this.tahun = tahun;
        return this;
    }
    public BadanAllDtoBuilder kodeKpp (String kodeKpp) {
        this.kodeKpp = kodeKpp;
        return this;
    }
    public BadanAllDtoBuilder namaKpp (String namaKpp) {
        this.namaKpp = namaKpp;
        return this;
    }
    public BadanAllDtoBuilder kanwil (String kanwil) {
        this.kanwil = kanwil;
        return this;
    }
    public BadanAllDtoBuilder provinsi (String provinsi) {
        this.provinsi = provinsi;
        return this;
    }
    public BadanAllDtoBuilder pulau (String pulau) {
        this.pulau = pulau;
        return this;
    }
    public BadanAllDtoBuilder kodeKategori (String kodeKategori) {
        this.kodeKategori = kodeKategori;
        return this;
    }
    public BadanAllDtoBuilder namaKategori (String namaKategori) {
        this.namaKategori = namaKategori;
        return this;
    }
    public BadanAllDtoBuilder kategoriWp (String kategoriWp) {
        this.kategoriWp = kategoriWp;
        return this;
    }
    public BadanAllDtoBuilder modal (String modal) {
        this.modal = modal;
        return this;
    }
    public BadanAllDtoBuilder statusPusat (String statusPusat) {
        this.statusPusat = statusPusat;
        return this;
    }
    public BadanAllDtoBuilder periodePembukuan (String periodePembukuan) {
        this.periodePembukuan = periodePembukuan;
        return this;
    }
    public BadanAllDtoBuilder tahunPendirian (String tahunPendirian) {
        this.tahunPendirian = tahunPendirian;
        return this;
    }
    public BadanAllDtoBuilder badanHukum (String badanHukum) {
        this.badanHukum = badanHukum;
        return this;
    }
    public BadanAllDtoBuilder jumlah (int jumlah) {
        this.jumlah = jumlah;
        return this;
    }
    public BadanAllDtoBuilder createdBy (String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    public BadanAllDtoBuilder createdDate (Date createdDate) {
        this.createdDate = new Date();
        return this;
    }

    public PenerimaanDto build() {
        PenerimaanDto penerimaanDto = new PenerimaanDto(this);
        return penerimaanDto;
    }
}