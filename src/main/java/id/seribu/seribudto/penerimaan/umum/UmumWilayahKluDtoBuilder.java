package id.seribu.seribudto.penerimaan.umum;

import java.util.Date;
import java.util.UUID;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import lombok.Getter;

@Getter
public class UmumWilayahKluDtoBuilder {
    
    private UUID id;
    private String tahun;
	private String kodeKpp;
	private String namaKpp;
	private String kanwil;
	private String provinsi;
	private String pulau;
	private String kodeKategori;
    private String namaKategori;
    private String jenisWp;
	private int jumlah;
	private String createdBy;
    private Date createdDate;

    public UmumWilayahKluDtoBuilder id() {
        this.id = UUID.randomUUID();
        return this;
    }

    public UmumWilayahKluDtoBuilder tahun(String tahun) {
        this.tahun = tahun;
        return this;
    }

    public UmumWilayahKluDtoBuilder kodeKpp(String kodeKpp) {
        this.kodeKpp = kodeKpp;
        return this;
    }

    public UmumWilayahKluDtoBuilder namaKpp(String namaKpp) {
        this.namaKpp = namaKpp;
        return this;
    }

    public UmumWilayahKluDtoBuilder kanwil(String kanwil) {
        this.kanwil = kanwil;
        return this;
    }

    public UmumWilayahKluDtoBuilder provinsi(String provinsi) {
        this.provinsi = provinsi;
        return this;
    }

    public UmumWilayahKluDtoBuilder pulau(String pulau) {
        this.pulau = pulau;
        return this;
    }

    public UmumWilayahKluDtoBuilder kodeKategori(String kodeKategori) {
        this.kodeKategori = kodeKategori;
        return this;
    }

    public UmumWilayahKluDtoBuilder namaKategori(String namaKategori) {
        this.namaKategori = namaKategori;
        return this;
    }

    public UmumWilayahKluDtoBuilder jenisWp(String jenisWp) {
        this.jenisWp = jenisWp;
        return this;
    }

    public UmumWilayahKluDtoBuilder jumlah(int jumlah) {
        this.jumlah = jumlah;
        return this;
    }

    public UmumWilayahKluDtoBuilder createdBy (String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    public UmumWilayahKluDtoBuilder createdDate (Date createdDate) {
        this.createdDate = new Date();
        return this;
    }

    public PenerimaanDto build() {
        PenerimaanDto penerimaanDto = new PenerimaanDto(this);
        return penerimaanDto;
    }
}