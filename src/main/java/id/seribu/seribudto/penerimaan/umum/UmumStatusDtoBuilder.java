package id.seribu.seribudto.penerimaan.umum;

import java.util.Date;
import java.util.UUID;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import lombok.Getter;

@Getter
public class UmumStatusDtoBuilder {

    private UUID id;
    private String tahun;
	private String kodeKpp;
	private String namaKpp;
	private String kanwil;
	private String provinsi;
	private String pulau;
	private String kodeKategori;
    private String namaKategori;
    private String statusNpwp;
    private String statusPkp;
    private String jenisWp;
	private int jumlah;
	private String createdBy;
    private Date createdDate;

    public UmumStatusDtoBuilder id() {
        this.id = UUID.randomUUID();
        return this;
    }

    public UmumStatusDtoBuilder tahun(String tahun) {
        this.tahun = tahun;
        return this;
    }

    public UmumStatusDtoBuilder kodeKpp(String kodeKpp) {
        this.kodeKpp = kodeKpp;
        return this;
    }

    public UmumStatusDtoBuilder namaKpp(String namaKpp) {
        this.namaKpp = namaKpp;
        return this;
    }

    public UmumStatusDtoBuilder kanwil(String kanwil) {
        this.kanwil = kanwil;
        return this;
    }

    public UmumStatusDtoBuilder provinsi(String provinsi) {
        this.provinsi = provinsi;
        return this;
    }

    public UmumStatusDtoBuilder pulau(String pulau) {
        this.pulau = pulau;
        return this;
    }

    public UmumStatusDtoBuilder kodeKategori(String kodeKategori) {
        this.kodeKategori = kodeKategori;
        return this;
    }

    public UmumStatusDtoBuilder namaKategori(String namaKategori) {
        this.namaKategori = namaKategori;
        return this;
    }

    public UmumStatusDtoBuilder statusNpwp(String statusNpwp) {
        this.statusNpwp = statusNpwp;
        return this;
    }

    public UmumStatusDtoBuilder statusPkp(String statusPkp) {
        this.statusPkp = statusPkp;
        return this;
    }

    public UmumStatusDtoBuilder jenisWp(String jenisWp) {
        this.jenisWp = jenisWp;
        return this;
    }

    public UmumStatusDtoBuilder jumlah(int jumlah) {
        this.jumlah = jumlah;
        return this;
    }

    public UmumStatusDtoBuilder createdBy (String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    public UmumStatusDtoBuilder createdDate (Date createdDate) {
        this.createdDate = new Date();
        return this;
    }

    public PenerimaanDto build() {
        PenerimaanDto penerimaanDto = new PenerimaanDto(this);
        return penerimaanDto;
    }
}