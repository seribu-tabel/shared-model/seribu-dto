package id.seribu.seribudto.penerimaan.bendahara;

import java.util.Date;
import java.util.UUID;

import id.seribu.seribudto.penerimaan.PenerimaanDto;
import lombok.Getter;

@Getter
public class BendaharaAllDtoBuilder {

    private UUID id;
	private String tahun;
	private String kodeKpp;
	private String namaKpp;
	private String kanwil;
	private String provinsi;
    private String pulau;
    private String jenis;
	private int jumlah;
	private String createdBy;
	private Date createdDate;

    public BendaharaAllDtoBuilder id() {
        this.id = UUID.randomUUID();
        return this;
    }
    public BendaharaAllDtoBuilder tahun (String tahun) {
        this.tahun = tahun;
        return this;
    }
    public BendaharaAllDtoBuilder kodeKpp (String kodeKpp) {
        this.kodeKpp = kodeKpp;
        return this;
    }
    public BendaharaAllDtoBuilder namaKpp (String namaKpp) {
        this.namaKpp = namaKpp;
        return this;
    }
    public BendaharaAllDtoBuilder kanwil (String kanwil) {
        this.kanwil = kanwil;
        return this;
    }
    public BendaharaAllDtoBuilder provinsi (String provinsi) {
        this.provinsi = provinsi;
        return this;
    }
    public BendaharaAllDtoBuilder pulau (String pulau) {
        this.pulau = pulau;
        return this;
    }
    public BendaharaAllDtoBuilder jenis (String jenis) {
        this.jenis = jenis;
        return this;
    }
    public BendaharaAllDtoBuilder jumlah (int jumlah) {
        this.jumlah = jumlah;
        return this;
    }
    public BendaharaAllDtoBuilder createdBy (String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    public BendaharaAllDtoBuilder createdDate (Date createdDate) {
        this.createdDate = new Date();
        return this;
    }

    public PenerimaanDto build() {
        PenerimaanDto PenerimaanDto = new PenerimaanDto(this);
        return PenerimaanDto;
    }
}