package id.seribu.seribudto.kepatuhan.op;

import java.util.Date;
import java.util.UUID;

import id.seribu.seribudto.kepatuhan.KepatuhanDto;
import lombok.Getter;

@Getter
public class OpKluDtoBuilder {
    private UUID id;
    private String tahun;
    private String kodeKategori;
    private String namaKategori;
	private String statusPerkawinan;
	private String jumlahTanggungan;
	private String jenisOp;
	private String rangeUsia;
	private String jenisKelamin;
	private String wargaNegara;
	private int jumlah;
	private String createdBy;
    private Date createdDate;
    
    public OpKluDtoBuilder id() {
        this.id = UUID.randomUUID();
        return this;
    }

    public OpKluDtoBuilder tahun(String tahun) {
        this.tahun = tahun;
        return this;
    }

    public OpKluDtoBuilder kodeKategori(String kodeKategori) {
        this.kodeKategori = kodeKategori;
        return this;
    }

    public OpKluDtoBuilder namaKategori (String namaKategori) {
        this.namaKategori = namaKategori;
        return this;        
    }

	public OpKluDtoBuilder statusPerkawinan (String statusPerkawinan) {
        this.statusPerkawinan = statusPerkawinan;
        return this;
    }
	public OpKluDtoBuilder jumlahTanggungan (String jumlahTanggungan) {
        this.jumlahTanggungan = jumlahTanggungan;
        return this;
    }
	public OpKluDtoBuilder jenisOp (String jenisOp) {
        this.jenisOp = jenisOp;
        return this;
    }
	public OpKluDtoBuilder rangeUsia (String rangeUsia) {
        this.rangeUsia = rangeUsia;
        return this;
    }
	public OpKluDtoBuilder jenisKelamin (String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
        return this;
    }
	public OpKluDtoBuilder wargaNegara (String wargaNegara) {
        this.wargaNegara = wargaNegara;
        return this;
    }
	public OpKluDtoBuilder jumlah (int jumlah) {
        this.jumlah = jumlah;
        return this;
    }
	public OpKluDtoBuilder createdBy (String createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    public OpKluDtoBuilder createdDate (Date createdDate) {
        this.createdDate = new Date();
        return this;
    } 

    public KepatuhanDto build() {
        KepatuhanDto kepatuhanDto = new KepatuhanDto(this);
        return kepatuhanDto;
    }
}