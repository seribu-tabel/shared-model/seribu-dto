package id.seribu.seribudto.kepatuhan;

import java.util.Date;
import java.util.UUID;

import id.seribu.seribudto.kepatuhan.badan.BadanAllDtoBuilder;
import id.seribu.seribudto.kepatuhan.badan.BadanStatusDtoBuilder;
import id.seribu.seribudto.kepatuhan.bendahara.BendaharaAllDtoBuilder;
import id.seribu.seribudto.kepatuhan.bendahara.BendaharaStatusDtoBuilder;
import id.seribu.seribudto.kepatuhan.op.OpKluDtoBuilder;
import id.seribu.seribudto.kepatuhan.op.OpStatusDtoBuilder;
import id.seribu.seribudto.kepatuhan.op.OpWilayahDtoBuilder;
import id.seribu.seribudto.kepatuhan.op.OpWilayahKluDtoBuilder;
import id.seribu.seribudto.kepatuhan.umum.UmumStatusDtoBuilder;
import id.seribu.seribudto.kepatuhan.umum.UmumWilayahKluDtoBuilder;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class KepatuhanDto {

    private UUID id;
	private String tahun;
	private String kodeKpp;
	private String namaKpp;
	private String kanwil;
	private String provinsi;
	private String pulau;
	private String kodeKategori;
	private String namaKategori;
	private String statusPerkawinan;
	private String jumlahTanggungan;
	private String jenisOp;
	private String rangeUsia;
	private String jenisKelamin;
	private String wargaNegara;
	private String kategoriWp;
	private String jenisWp;
	private String modal;
	private String statusPusat;
	private String periodePembukuan;
	private String tahunPendirian;
	private String badanHukum;
	private String jenis;
	private String statusNpwp;
	private String statusPkp;
	private String metodePembukuan;
	private int jumlah;
	private String createdBy;
	private Date createdDate;

	public KepatuhanDto(OpWilayahDtoBuilder builder) {
		this.id = builder.getId();
		this.tahun = builder.getTahun();
		this.kodeKpp = builder.getKodeKpp();
		this.namaKpp = builder.getNamaKpp();
		this.kanwil = builder.getKanwil();
		this.provinsi = builder.getProvinsi();
		this.pulau = builder.getPulau();
		this.statusPerkawinan = builder.getStatusPerkawinan();
		this.jumlahTanggungan = builder.getJumlahTanggungan();
		this.jenisOp = builder.getJenisOp();
		this.rangeUsia = builder.getRangeUsia();
		this.jenisKelamin = builder.getJenisKelamin();
		this.wargaNegara = builder.getWargaNegara();
		this.jumlah = builder.getJumlah();
		this.createdBy = builder.getCreatedBy();
    	this.createdDate = builder.getCreatedDate();
	}

	public KepatuhanDto(OpWilayahKluDtoBuilder builder) {
		this.id = builder.getId();
		this.tahun = builder.getTahun();
		this.kodeKpp = builder.getKodeKpp();
		this.namaKpp = builder.getNamaKpp();
		this.kanwil = builder.getKanwil();
		this.provinsi = builder.getProvinsi();
		this.pulau = builder.getPulau();
		this.kodeKategori = builder.getKodeKategori();
		this.namaKategori = builder.getNamaKategori();
		this.jumlah = builder.getJumlah();
		this.createdBy = builder.getCreatedBy();
		this.createdDate = builder.getCreatedDate();
	}

	public KepatuhanDto(OpKluDtoBuilder builder) {
		this.id = builder.getId();
		this.tahun = builder.getTahun();
		this.kodeKategori = builder.getKodeKategori();
		this.namaKategori = builder.getNamaKategori();
		this.statusPerkawinan = builder.getStatusPerkawinan();
		this.jumlahTanggungan = builder.getJumlahTanggungan();
		this.jenisOp = builder.getJenisOp();
		this.rangeUsia = builder.getRangeUsia();
		this.jenisKelamin = builder.getJenisKelamin();
		this.wargaNegara = builder.getWargaNegara();
		this.jumlah = builder.getJumlah();
		this.createdBy = builder.getCreatedBy();
		this.createdDate = builder.getCreatedDate();
	}

	public KepatuhanDto(OpStatusDtoBuilder builder) {
		this.id = builder.getId();
		this.tahun = builder.getTahun();
		this.kodeKpp = builder.getKodeKpp();
		this.namaKpp = builder.getNamaKpp();
		this.kanwil = builder.getKanwil();
		this.provinsi = builder.getProvinsi();
		this.pulau = builder.getPulau();
		this.kodeKategori = builder.getKodeKategori();
		this.namaKategori = builder.getNamaKategori();
		this.statusNpwp = builder.getStatusNpwp();
		this.statusPkp = builder.getStatusPkp();
		this.jumlah = builder.getJumlah();
		this.createdBy = builder.getCreatedBy();
		this.createdDate = builder.getCreatedDate();
	}

	public KepatuhanDto(BadanStatusDtoBuilder builder) {
		this.id = builder.getId();
		this.tahun = builder.getTahun();
		this.kodeKpp = builder.getKodeKpp();
		this.namaKpp = builder.getNamaKpp();
		this.kanwil = builder.getKanwil();
		this.provinsi = builder.getProvinsi();
		this.pulau = builder.getPulau();
		this.kodeKategori = builder.getKodeKategori();
		this.namaKategori = builder.getNamaKategori();
		this.kategoriWp = builder.getKategoriWp();
		this.modal = builder.getModal();
		this.statusPusat = builder.getStatusPusat();
		this.periodePembukuan = builder.getPeriodePembukuan();
		this.tahunPendirian = builder.getTahunPendirian();
		this.badanHukum = builder.getBadanHukum();
		this.statusNpwp = builder.getStatusNpwp();
		this.statusPkp = builder.getStatusPkp();
		this.jumlah = builder.getJumlah();
		this.createdBy = builder.getCreatedBy();
		this.createdDate = builder.getCreatedDate();
	}

	public KepatuhanDto(BadanAllDtoBuilder builder) {
		this.id = builder.getId();
		this.tahun = builder.getTahun();
		this.kodeKpp = builder.getKodeKpp();
		this.namaKpp = builder.getNamaKpp();
		this.kanwil = builder.getKanwil();
		this.provinsi = builder.getProvinsi();
		this.pulau = builder.getPulau();
		this.kodeKategori = builder.getKodeKategori();
		this.namaKategori = builder.getNamaKategori();
		this.kategoriWp = builder.getKategoriWp();
		this.modal = builder.getModal();
		this.statusPusat = builder.getStatusPusat();
		this.periodePembukuan = builder.getPeriodePembukuan();
		this.tahunPendirian = builder.getTahunPendirian();
		this.badanHukum = builder.getBadanHukum();
		this.jumlah = builder.getJumlah();
		this.createdBy = builder.getCreatedBy();
		this.createdDate = builder.getCreatedDate();
	}

	public KepatuhanDto(BendaharaAllDtoBuilder builder) {
		this.id = builder.getId();
		this.tahun = builder.getTahun();
		this.kodeKpp = builder.getKodeKpp();
		this.namaKpp = builder.getNamaKpp();
		this.kanwil = builder.getKanwil();
		this.provinsi = builder.getProvinsi();
		this.pulau = builder.getPulau();
		this.jenis = builder.getJenis();
		this.jumlah = builder.getJumlah();
		this.createdBy = builder.getCreatedBy();
		this.createdDate = builder.getCreatedDate();
	}

	public KepatuhanDto(BendaharaStatusDtoBuilder builder) {
		this.id = builder.getId();
		this.tahun = builder.getTahun();
		this.kodeKpp = builder.getKodeKpp();
		this.namaKpp = builder.getNamaKpp();
		this.kanwil = builder.getKanwil();
		this.provinsi = builder.getProvinsi();
		this.pulau = builder.getPulau();
		this.jenis = builder.getJenis();
		this.statusNpwp = builder.getStatusNpwp();
		this.statusPkp = builder.getStatusPkp();
		this.jumlah = builder.getJumlah();
		this.createdBy = builder.getCreatedBy();
		this.createdDate = builder.getCreatedDate();
	}

	public KepatuhanDto(UmumWilayahKluDtoBuilder builder) {
		this.id = builder.getId();
		this.tahun = builder.getTahun();
		this.kodeKpp = builder.getKodeKpp();
		this.namaKpp = builder.getNamaKpp();
		this.kanwil = builder.getKanwil();
		this.provinsi = builder.getProvinsi();
		this.pulau = builder.getPulau();
		this.kodeKategori = builder.getKodeKategori();
		this.namaKategori = builder.getNamaKategori();
		this.jenisWp = builder.getJenisWp();
		this.jumlah = builder.getJumlah();
		this.createdBy = builder.getCreatedBy();
		this.createdDate = builder.getCreatedDate();
	}

	public KepatuhanDto(UmumStatusDtoBuilder builder) {
		this.id = builder.getId();
		this.tahun = builder.getTahun();
		this.kodeKpp = builder.getKodeKpp();
		this.namaKpp = builder.getNamaKpp();
		this.kanwil = builder.getKanwil();
		this.provinsi = builder.getProvinsi();
		this.pulau = builder.getPulau();
		this.kodeKategori = builder.getKodeKategori();
		this.namaKategori = builder.getNamaKategori();
		this.statusNpwp = builder.getStatusNpwp();
		this.statusPkp = builder.getStatusPkp();
		this.jenisWp = builder.getJenisWp();
		this.jumlah = builder.getJumlah();
		this.createdBy = builder.getCreatedBy();
		this.createdDate = builder.getCreatedDate();
	}
}